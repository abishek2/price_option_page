import React from "react";
import "./footer.css";
import  {FooterDataType}  from "../../types";

interface Props{
  footerItem:FooterDataType,
}

export const Footer:React.FC<Props>=({footerItem})=>{
  return <div className="footerSectionItem">
    <h4 className="titleFooter">{footerItem.title}</h4>
    <div className="listItems">
      {
        footerItem.items.map((item)=>{
          return <div>{item}</div>
        })  
      }
    </div>
  </div>
};