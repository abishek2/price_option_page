import React from "react";
import "./main.css";
import "../price_plan/price_plan.css"
import { PricePlanType, FooterDataType } from "../../types";
import { Footer } from "../footer/footer";
import { AiOutlineStar } from "react-icons/ai";


const pricePlans: PricePlanType[] = [
  {
    id: 1,
    title: "Free",
    price: 0,
    features: ["10 users included", "2 GB of Storage", "Help center access", "Email support"],
    button_label: "sign up for free",
    isPopular: false,
  },
  {
    id: 2,
    title: "Pro",
    price: 15,
    features: ["20 users included", "10 GB of storage", "Help center access", "Priority email support"],
    button_label: "get started",
    isPopular: true,
  },
  {
    id: 3,
    title: "Enterprise",
    price: 30,
    features: ["50 users included", "30 GB of storage", "Help center access", "Phone & email support"],
    button_label: "contact us",
    isPopular: false,
  }
];

const footerData: FooterDataType[] = [
  {
    title: "Company",
    items: ["Team", "History", "Contuct us", "Locatuion"],
  },
  {
    title: "Features",
    items: ["Cool stuff", "Random feature", "Team feature", "Developer stuff", "Another one"],
  },
  {
    title: "Resources",
    items: ["Resource", "Resource name", "Another resource", "Final resource"],
  },
  {
    title: "Legal",
    items: ["Privacy policY", "Terms of use"],
  },
];
interface Props {
  isPopular: boolean[],
}
interface State {
  isPopular: boolean[],
}

class Main extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);
    this.state = { isPopular: props.isPopular };
    //console.log(this.state.isPopular);
  }

  toggleState(index: number) {
    let tempArr = [false, false, false];
    tempArr[index] = true;
    this.setState({ isPopular: tempArr });
  }
  render() {
    console.log("Main render");
    return (<div className="mainClass">
      <h1>Pricing</h1>

      <p>Quickly build an effective pricing table for your potential <br />
    customers with this layout. It's built with default <br />
    Material-UI components with little customization
    </p>

      <ul className="pricePlan">
        {
          pricePlans.map((value, index, pricePlanArr) => (
            <li onClick={()=>this.toggleState(index)} key={value.id} className="pricePlanItem">

              <div className={`titlePricePlan titlePricePlan${this.state.isPopular[index]}`}>
                <div className="starIcon">{this.state.isPopular[index] ? <AiOutlineStar /> : null}</div>
                <div className="pricePlanTitleText">{value.title}</div>
                <div className="popularTag">{this.state.isPopular[index] ? "Most Popular" : null}</div>
              </div>


              <div className="priceTag">
                <span className="planPrice">${value.price}</span>
                <span className="planPeriod">/mo</span>
              </div>
              <section className="features">
                {
                  value.features.map((feature) => {
                    return <div className="featureItem">{feature}</div>;
                  })
                }
              </section>
              <button className={`${this.state.isPopular[index] ? "buttonTrue" : "buttonFalse"} buttonSignup`}>{value.button_label}</button>
            </li>
          ))
        }
      </ul>

      <section className="footerSection">
        {
          footerData.map((footerItem) => {
            return <Footer footerItem={footerItem} key={footerItem.title} />
          })
        }
      </section>
    </div>);
  }
}

export default Main;