import Main from "./components/main/main";

const isPopular:boolean[]=[false,true,false];
function App() {
  return (
    <Main isPopular={isPopular} />
  );
}

export default App;
