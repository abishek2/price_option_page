export interface PricePlanType{
  id:number,
  title:string,
  price:number,
  features:string[],
  button_label:string,
  isPopular:boolean,
};
export interface FooterDataType{
  title:string,
  items:string[],
}